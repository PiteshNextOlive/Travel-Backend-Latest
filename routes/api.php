<?php

use App\Http\Controllers\AirportController;
use App\Http\Controllers\BoatController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\DuffelController;
use App\Http\Controllers\FlyEasyController;
use App\Http\Controllers\GetARoomController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HostwayController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'api.'], function(){
    Route::any('test', [HomeController::class,'test']);
    Route::group(['prefix' => 'user','as' => 'user.'], function(){
        Route::post('login', [UserController::class,'login'])->name('login');
        Route::post('logout', [UserController::class,'logout'])->name('logout')->middleware('auth:sanctum');
    });

    Route::group(['prefix' => 'get-a-room','as' => 'get-a-room.','middleware' => 'auth:sanctum'], function(){
        Route::group(['prefix' => 'property', 'as' => 'property.'], function(){
            Route::post('index', [GetARoomController::class,'propertyList'])->name('index');
            Route::post('index/{id}', [GetARoomController::class,'propertyListById'])->name('propertyListById');
        });
    });

    Route::group(['prefix' => 'fle','as' => 'flyeasy.',], function(){
        Route::post('emptylegs/search', [FlyEasyController::class,'emptyLegSearch'])->name('emptylegs.search');
    });

    Route::group(['prefix' => 'duffel','as' => 'duffel.',], function(){
        Route::post('offer_requests', [DuffelController::class,'offer_requests'])->name('offer_requests');
        Route::post('offer_request', [DuffelController::class,'offer_request'])->name('offer_request');
    });

    Route::group(['prefix' => 'hostway','as' => 'hostway.','middleware' => 'auth:sanctum'], function(){
        Route::post('token', [HostwayController::class,'accessToken'])->name('token');
        Route::post('listings', [HostwayController::class,'listings'])->name('listings');
    });

    Route::group(['prefix' => 'airport','as' => 'airport.',], function(){
        Route::post('list', [AirportController::class,'index'])->name('list');
    });

    Route::group(['prefix' => 'car','as' => 'car.',], function(){
        Route::post('list', [CarController::class,'apiIndex'])->name('list');
    });

    Route::group(['prefix' => 'boat','as' => 'boat.',], function(){
        Route::post('list', [BoatController::class,'apiIndex'])->name('list');
    });

    Route::group(['prefix' => 'location','as' => 'location.',], function(){
        Route::group(['prefix' => 'country','as' => 'country.',], function(){
            Route::post('', [LocationController::class,'listCountry'])->name('list');
        });
        Route::group(['prefix' => 'state','as' => 'state.',], function(){
            Route::post('', [LocationController::class,'listState'])->name('list');
        });
        Route::group(['prefix' => 'city','as' => 'city.',], function(){
            Route::post('', [LocationController::class,'listCity'])->name('list');
        });
    });
});
