<?php

use App\Http\Controllers\BoatController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VillaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::any('/', [HomeController::class,'index_view']);
Route::get('get-a-room-api-fetch', [HomeController::class,'getARoomApiFetch']);
Route::get('get-a-room-seed', [HomeController::class,'getARoomSeed']);
Route::get('home', [HomeController::class,'index_view'])->name('home')->middleware('auth');
Route::get('login', [HomeController::class,'login_view'])->name('login.view')->middleware('guest');
Route::post('login', [HomeController::class,'login'])->name('login.store')->middleware('guest');
Route::get('signup', [HomeController::class,'signup_view'])->name('signup.view')->middleware('guest');
Route::post('signup', [HomeController::class,'signup_store'])->name('signup.store');
Route::get('logout', [HomeController::class,'logout'])->name('logout');


Route::group(['middleware' => 'auth',], function(){
    Route::get('dashboard', [HomeController::class,'index_view'])->name('dashboard.view')->middleware('auth');

    Route::group(['prefix' => 'car','as' => 'car.',], function(){
        Route::group(['prefix' => 'feature','as' => 'feature.',], function(){
            Route::get('create', [CarController::class,'createCarFeatureView'])->name('create.view');
            Route::post('store', [CarController::class,'createCarFeature'])->name('create');
            Route::get('list', [CarController::class,'listCarFeature'])->name('list.view');
        });
        Route::get('create', [CarController::class,'createView'])->name('create.view');
        Route::post('store', [CarController::class,'create'])->name('create');
        Route::post('import', [CarController::class,'import'])->name('import');
        Route::get('list', [CarController::class,'listView'])->name('list.view');
        Route::get('delete', [CarController::class,'delete'])->name('delete');
    });

    Route::group(['prefix' => 'boat','as' => 'boat.',], function(){
        Route::get('create', [BoatController::class,'createView'])->name('create.view');
        Route::post('store', [BoatController::class,'create'])->name('create');
        Route::post('import', [BoatController::class,'import'])->name('import');
        Route::get('list', [BoatController::class,'listView'])->name('list.view');
        Route::get('delete', [BoatController::class,'delete'])->name('delete');
    });

    Route::group(['prefix' => 'villa','as' => 'villa.',], function(){
        Route::get('create', [VillaController::class,'createView'])->name('create.view');
        Route::post('store', [VillaController::class,'create'])->name('create');
//        Route::post('import', [BoatController::class,'import'])->name('import');
        Route::get('list', [VillaController::class,'listView'])->name('list.view');
//        Route::get('delete', [BoatController::class,'delete'])->name('delete');
    });



});
