<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (\Exception $e, $request) {
            if ($request->is('*api/*')||$request->expectsJson()) {
                // In case of send it always as json
                return respond(false,$e->getMessage(), $e);
            }
            else{
                if ($e instanceof ValidationException) {
                    return redirect()->back()->withInput($request->input())->withErrors($e->validator->errors());
                }
                if ($e instanceof AuthenticationException) {
                    return redirect()->route('login');
                }
            }
        });
    }
}
