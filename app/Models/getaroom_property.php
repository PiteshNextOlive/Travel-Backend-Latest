<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class getaroom_property extends Model
{
    protected $fillable=['uuid','permalink','title','lat','lng','short-description','sanitized-description','rating',
        'location-phone',
        'location-street',
        'location-city',
        'location-state',
        'location-zip',
        'location-country',
        'amenities',
        'thumbnail-filename'
    ];
    use HasFactory;

    protected function permalink(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function title(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function lat(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function lng(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function shortDescription(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function sanitizedDescription(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function rating(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function locationPhone(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function locationStreet(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function locationCity(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function locationState(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function locationZip(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function locationCountry(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function amenities(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
    protected function thumbnailFilename(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => (gettype($value)=='object')?null:$value,
        );
    }
}
