<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Car extends Model
{
    protected $fillable=['title','slug','content','status','type','features','location','address','youtube_link','vin',
        'latitude','longitude','base_price_daily','price_mod_sign_daily','price_mod_type_daily','price_mod_amount_daily','base_price_weekly',
        'price_mod_sign_weekly','price_mod_type_weekly','price_mod_amount_weekly','base_price_monthly','price_mod_sign_monthly','price_mod_type_monthly',
        'price_mod_amount_monthly','gear_type','door','specification','faq'
        ];
    use HasFactory;
    protected $appends=['media','calculated_price'];
    protected function specification(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ($value)? json_decode($value):$value,
        );
    }
    protected function faq(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ($value)? json_decode($value):$value,
        );
    }
    protected function features(): Attribute
    {
        return Attribute::make(function ($value) {
            $returnable=[];
            if ($value){
                $arr=json_decode($value);
                for ($i = 0; $i < count($arr); $i++) {
                    $returnable[]=Extra::where(['identifier' => 'CAR_FEATURE','id' => $arr[$i]])->first(['id','value']);
                }
            }
            return (count($returnable))?$returnable:$value;
        });
    }
    public function getMediaAttribute()
    {
        $returnable=[];
        $returnable['images']=Media::where(['reference_code'=>'CAR_IMAGES','refrence_id'=>$this->id])->get();
        return $returnable;
    }
    public function getCalculatedPriceAttribute()
    {
        $returnable=new \stdClass();
        $returnable->daily=calPrice($this->base_price_daily,$this->price_mod_type_daily,$this->price_mod_amount_daily);
        $returnable->weekly=calPrice($this->base_price_weekly,$this->price_mod_type_weekly,$this->price_mod_amount_weekly);
        $returnable->monthly=calPrice($this->base_price_monthly,$this->price_mod_type_monthly,$this->price_mod_amount_monthly);
        return $returnable;
    }

    public function location(): HasOne
    {
        return $this->hasOne(Location::class,'id','location');
    }
}
