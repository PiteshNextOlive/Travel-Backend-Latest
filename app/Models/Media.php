<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;
    protected $fillable=['path','is_local','reference_code','refrence_id','additional','created_by','created_at','updated_at'];
}
