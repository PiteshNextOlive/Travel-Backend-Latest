<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Villa extends Model
{
    use HasFactory;
    protected $appends=['media'];
    public function getMediaAttribute()
    {
        $returnable=[];
        $returnable['images']=Media::where(['reference_code'=>config('app.VILLA_IMAGES'),'refrence_id'=>$this->id])->get();
        return $returnable;
    }
}
