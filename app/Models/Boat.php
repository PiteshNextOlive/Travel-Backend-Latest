<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boat extends Model
{
    use HasFactory;
    protected $fillable=['title','slug','content','status','type','features','location','address','youtube_link','vin',
        'latitude','longitude','base_price_daily','price_mod_sign_daily','price_mod_type_daily','price_mod_amount_daily','base_price_weekly',
        'price_mod_sign_weekly','price_mod_type_weekly','price_mod_amount_weekly','base_price_monthly','price_mod_sign_monthly','price_mod_type_monthly',
        'price_mod_amount_monthly','specification','faq'
    ];
    protected $appends=['media','calculated_price'];

    protected function faq(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ($value)? json_decode($value):$value,
        );
    }
    protected function specification(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => ($value)? json_decode($value):$value,
        );
    }
    public function getCalculatedPriceAttribute()
    {
        $returnable=new \stdClass();
        $returnable->daily=calPrice($this->base_price_daily,$this->price_mod_type_daily,$this->price_mod_amount_daily);
        $returnable->weekly=calPrice($this->base_price_weekly,$this->price_mod_type_weekly,$this->price_mod_amount_weekly);
        $returnable->monthly=calPrice($this->base_price_monthly,$this->price_mod_type_monthly,$this->price_mod_amount_monthly);
        return $returnable;
    }

    public function getMediaAttribute()
    {
        $returnable=[];
        $returnable['images']=Media::where(['reference_code'=> config('app.BOAT_IMAGES'),'refrence_id'=>$this->id])->get();
        return $returnable;
    }
}
