<?php
function respond($status=null, $message=null, $data=null, $statusCode=200): \Illuminate\Http\JsonResponse
{
    $status=(bool)$status;
    if(!$message){
        $message=($status)?"Action performed successfully!":"Unable to perform this action";
    }
    return response()->json(['status' => $status, 'message' => $message, 'data' => $data ],$statusCode);
}
function calPrice($basePrice,$modType,$modAmount,$modSign='-')
{
    $r=null;
    if ($modSign=='-'){
        if (strtolower($modType)=='f'){
            $r=$basePrice-$modAmount;
        }
        if (strtolower($modType)=='p'){
            $r=$basePrice-($basePrice*$modAmount/100);
        }
    }
    return $r;
}
