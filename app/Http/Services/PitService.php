<?php

namespace App\Http\Services;

use Http;

class PitService
{
    protected $model;
    public function __construct($model)
    {
        $this->model = $model;
    }
    public function index($request,$customPost=[])
    {
        $_select=null;
        $_populate=null;
        $_role=null;
        if($request->has('_select')){
            $_select=$request->_select;
            unset($request['_select']);
        }
        //Means Relation code Data
        if($request->has('_populate')){
            $_populate=$request->_populate;
            unset($request['_populate']);
        }
        //Means Relation code Role Data
        if($request->has('_role')){
            $_role=$request->_role;
            unset($request['_role']);
        }
        //Order Data
        if($request->has('_order')){
            $_order=$request->_order;
            unset($request['_order']);
        }
        $d = $this->model::where($request->post());
        if ($customPost){
            $d->where($customPost);
        }
        //For Relation code data
        if($_populate){
            $d=$d->with(explode(',',$_populate));
        }
        //For Relation code data
        if($_role){
            $d=$d->role(explode(',',$_role));
        }
        if($_populate && $_select){
            throw new \Exception("Please provide either populate or select at once");
        }
        //For Select code data
        if($_select){
            $d=$d->select(explode(',',$_select));
        }

        return ($d->count()>0)?$d->paginate($request->query()['paginate']?? config('app.per_page')):null;
    }
    public function create($data)
    {
        return $this->model::create($data->post());
    }
    public function edit($id)
    {
        return $this->model::findOrFail($id);
    }
    public function update($data)
    {
        $result = $this->model::findOrFail($data->id)->update($data->post());
        if($result){
            $project = $this->model::findOrFail($data->id);
        }else{
            $project = [];
        }
        return $project;
    }
    public function delete($data)
    {
        return $this->model::findOrFail($data->id)->delete();
    }
    function fetchPost($arrHeaders,$endpoint,$arrBody=[]): \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
    {
        return Http::withoutVerifying()->asForm()->withHeaders($arrHeaders)
            ->post($endpoint, $arrBody);
    }
    function fetchGet($arrHeaders,$endpoint,$queryParams=[]): \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
    {
        return Http::withoutVerifying()->withHeaders($arrHeaders)
            ->get($endpoint, $queryParams);
    }

}
