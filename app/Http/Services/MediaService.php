<?php

namespace App\Http\Services;

use App\Models\Media;
use Storage;

class MediaService
{
    public function delete(Array $ids){
        $arrResult = array();
        foreach($ids as $id){
            $m=Media::find($id);
            $r=\File::delete('public/'.$m->path);
            $m=$m->delete();
            $arrResult[]=$r&&$m;
        }
        return $arrResult;
    }
    public function upload($file,$path,$reference_code=null,$reference_id=null,$additional=null,$created_by=null){
        // When $file is a file
        $path=Storage::disk('my_public')->put($path, $file);
        if (!$path){
            return false;
        }
        $d= Media::create([
            'path'=>'public/uploads/'.$path ,
            'reference_code'=>$reference_code,
            'refrence_id'=>$reference_id,
            'additional' => $additional,
            'created_by'=>($created_by==null&&auth()->user())?auth()->user()->id:$created_by
        ]);
        return $d;
    }
    public function write($file,$path,$reference_code=null,$reference_id=null,$additional=null,$created_by=null){
        // When $file is not file (to be written as html css or text file
        Storage::disk('my_public')->put($path, $file);
        if (!$path){
            return false;
        }
        $d= Media::create([
            'path'=>'uploads/'.$path ,
            'reference_code'=>$reference_code,
            'refrence_id'=>$reference_id,
            'additional' => $additional,
            'created_by'=>($created_by==null&&auth()->user())?auth()->user()->id:$created_by
        ]);
        return $d;
    }
}
