<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetARoomRequest;
use App\Http\Services\PitService;
use App\Models\getaroom_property;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GetARoomController extends Controller
{
    function propertyList(Request $request)
    {
        $cs = new PitService(getaroom_property::class);
        $d = $cs->index($request);
        return respond((bool)$d,null,$d);
    }
    function propertyListById(Request $request,$id)
    {
        if (!$request->id){
            return respond(false,'id is required');
        }
        $ps=new PitService(null);
        $d=$ps->fetchGet([
            'Authorization' => 'Basic '.base64_encode(config('app.GETAROOM_API_KEY').':'.config('app.GETAROOM_AUTH_TOKEN')),
            'Accept' => 'application/json',
        ],'https://supply.integration2.testaroom.com/hotel/api/properties/'.$id);
        return $d->collect();
    }
}
