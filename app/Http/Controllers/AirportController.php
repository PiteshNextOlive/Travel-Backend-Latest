<?php

namespace App\Http\Controllers;

use App\Http\Services\PitService;
use App\Models\Airport;
use Illuminate\Http\Request;

class AirportController extends Controller
{
    public function index(Request $request)
    {
        $cs = new PitService(Airport::class);
        $d = $cs->index($request);
        return respond((bool)$d,null,$d);
    }
}
