<?php

namespace App\Http\Controllers;

use App\Http\Services\MediaService;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Models\Villa;
use Illuminate\Http\Request;

class VillaController extends Controller
{
    function createView()
    {
        $countries=Country::all('name','id');
//        $states=State::all('name','id');
//        $cities=City::all('name','id');
        foreach ($countries as $country) {
            $country['value']=$country->name;
        }
//        foreach ($states as $s) {
//            $s['value']=$s->name;
//        }
//        foreach ($cities as $c) {
//            $c['value']=$c->name;
//        }
        return view('villa.create',['countries'=>$countries,]);
    }
    function create(Request $request)
    {
       $validation=$request->validate([
           'name'=>'required',
           'country'=>'required',
           'state'=>'required',
           'city'=>'required'
       ]);

       $countryId = json_decode($request->country, true)[0]['id'];
       if(!empty($request->listingAmenities)){
        $listingAmenities = collect(json_decode($request->listingAmenities))->pluck('value');
       }else{
        $listingAmenities = NULL;
       }

       $villa=new Villa();
       $villa->name=$request->name;
       $villa->description=$request->description;
       $villa->houseRules=$request->houseRules;
       $villa->country= $countryId;
       $villa->state= $request->state;
       $villa->city= $request->city;
       $villa->countryCode='en'; //Static data
       $villa->zipcode=$request->zipcode;
       $villa->price=$request->price;
       $villa->starRating=$request->starRating;
       $villa->weeklyDiscount=$request->weeklyDiscount;

       $villa->monthlyDiscount=$request->monthlyDiscount;
       $villa->address=$request->address;
       $villa->lat=$request->lat;
       $villa->lng=$request->lng;

       $villa->roomType= 'entire_home'; //Static data
       $villa->language='en'; //Static data
       $villa->currencyCode='US'; //Static data
       $villa->listingAmenities= $listingAmenities;

       if (!$villa->save()){
           return redirect()->back()->with('error',config('app.NEGATIVE_MESSAGE'));
       }

       if ($request->hasFile('listingImages')){
           $ms=new MediaService();
           foreach ($request->listingImages as $image){
               $ms->upload($image,'villas',config('app.VILLA_IMAGES'),$villa->id);
           }
       }
        return redirect()->route('villa.list.view')->with('success',config('app.POSITIVE_MESSAGE'));
    }
    function listView()
    {
        $villas = Villa::get();
        return view('villa.index', compact('villas'));
    }
}
