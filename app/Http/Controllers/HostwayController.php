<?php

namespace App\Http\Controllers;

use App\Http\Requests\HostwayRequest;
use App\Http\Services\PitService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HostwayController extends Controller
{
    function accessToken(Request $request)
    {
        $ps=new PitService(null);
        $r=$ps->fetchPost([],config('app.HOSTWAY_ENDPOINT').'accessTokens',[
            'grant_type'=>'client_credentials','client_id'=>config('app.HOSTWAY_CLIENT_ID'),'client_secret'=>config('app.HOSTWAY_CLIENT_SECRET'),'scope'=>'general'
        ]);
        return $r->collect();
    }
    function listings(HostwayRequest $request)
    {
        $ps=new PitService(null);
        $r=$ps->fetchGet(['Authorization'=>'Bearer '.$request->tkn],config('app.HOSTWAY_ENDPOINT').'listings/'.($request->id??null),[]);
        return $r->collect();
    }
}
