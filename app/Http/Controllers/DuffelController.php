<?php

namespace App\Http\Controllers;

use App\Http\Requests\DuffelRequest;
use App\Http\Services\PitService;
use Http;
use Illuminate\Http\Request;

class DuffelController extends Controller
{
    function offer_requests(Request $request)
    {
        $d='{
    "data": {
        "slices": [
            {
                "origin": "DEL",
                "destination": "ATL",
                "departure_date": "2024-06-21"
            },
            {
                "origin": "ATL",
                "destination": "NYC",
                "departure_date": "2024-07-21"
            }
        ],
        "passengers": [
            {
                "type": "adult"
            },
            {
                "type": "adult"
            },
            {
                "age": 1
            }
        ],
        "cabin_class": "business"
    }
}';
       $s=new PitService(null);
        $r=$s->fetchPost(
            ['Authorization'=>'Bearer '.config('app.DUFFEL_KEY'), 'Duffel-Version'=>'v1']
            ,config('app.DUFFEL_ENDPOINT').'offer_requests'
            ,json_decode($d)
        );
        return $r->collect();
    }
    function offer_request(DuffelRequest $request)
    {
        return config('app.DUFFEL_ENDPOINT').'offers/'.$request->id;
       $s=new PitService(null);
        $r=$s->fetchPost(
            ['Authorization'=>'Bearer '.config('app.DUFFEL_KEY'), 'Duffel-Version'=>'v1']
            ,config('app.DUFFEL_ENDPOINT').'offers/'.$request->id
        );
        return $r->collect();
    }
}
