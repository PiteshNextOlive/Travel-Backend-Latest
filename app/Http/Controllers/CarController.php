<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRequest;
use App\Http\Services\MediaService;
use App\Http\Services\PitService;
use App\Imports\CarImport;
use App\Models\Car;
use App\Models\Extra;
use App\Models\Location;
use App\Models\Media;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CarController extends Controller
{
    function createView()
    {
        $carsToMark=Extra::where(['identifier' => 'PENDING_CAR_UPLOAD','created_by' => auth()->user()->id])->get();
        foreach ($carsToMark as $c) {
            $car=Car::where(['slug' => $c->value])->first();
            $media=Media::where(['id' => $c->additional])->first()->update(['refrence_id' => $car->id]);
        }
        Extra::whereIn('id',$carsToMark->pluck('id'))->delete();
        $locations=Location::all();
        $features=Extra::where(['identifier' => config('app.CAR_FEATURE')])->get(['value','id']);
        return view('car.create',['locations'=>$locations,'features'=>$features]);
    }
    function apiIndex(Request $request)
    {
        $cs = new PitService(Car::class);
        $d = $cs->index($request);
        return respond((bool)$d,null,$d);
    }
    function create(Request $request)
    {
        $validation=$request->validate([
            'title'=>'required',
            'content'=>'required',
// TODO: Car type
//            'type'=>'required',
            'features'=>'required',
            'location'=>'required|exists:locations,id',
            'address'=>'required',

            'base_price_daily'=>'required|numeric',
            'price_mod_type_daily'=>'in:p,f',
            'price_mod_amount_daily'=>'required|numeric',

            'base_price_weekly'=>'required|numeric',
            'price_mod_type_weekly'=>'in:p,f',
            'price_mod_amount_weekly'=>'required|numeric',

            'base_price_monthly'=>'required|numeric',
            'price_mod_type_monthly'=>'in:p,f',
            'price_mod_amount_monthly'=>'required|numeric',

            'gear_type'=>'in:automatic,manual',
            'door'=>'numeric',
        ]);

        $faqjson=$specificationJson=[];
        for ($i = 0; $i < count($request->faq_q); $i++) {
            $faqjson[$i][$request->faq_q[$i]]=$request->faq_a[$i];
        }
        for ($i = 0; $i < count($request->specification_k); $i++) {
            $specificationJson[$i][$request->specification_k[$i]]=$request->specification_v[$i];
        }
        $faqjson=json_encode($faqjson);
        $specificationJson=json_encode($specificationJson);

        $car=new Car();
        $car->title=$request->title;
        $car->slug=\Str::slug($request->title);
        $car->content=$request->post('content');
        $car->features=collect(json_decode($request->features))->pluck('id');
        $car->location=$request->location;
        $car->address=$request->address;
        $car->youtube_link=$request->youtube_link;
        $car->vin=$request->vin;
        $car->latitude=$request->latitude;
        $car->longitude=$request->longitude;
        $car->base_price_daily=$request->base_price_daily;
        $car->price_mod_sign_daily='-';
        $car->price_mod_type_daily=$request->price_mod_type_daily;
        $car->price_mod_amount_daily=$request->price_mod_amount_daily;

        $car->base_price_weekly=$request->base_price_weekly;
        $car->price_mod_sign_weekly='-';
        $car->price_mod_type_weekly=$request->price_mod_type_weekly;
        $car->price_mod_amount_weekly=$request->price_mod_amount_weekly;

        $car->base_price_monthly=$request->base_price_monthly;
        $car->price_mod_sign_monthly='-';
        $car->price_mod_type_monthly=$request->price_mod_type_monthly;
        $car->price_mod_amount_monthly=$request->price_mod_amount_monthly;

        $car->gear_type=$request->gear_type;
        $car->door=$request->door;
        $car->faq=$faqjson;
        $car->specification=$specificationJson;
        if (!$car->save()){
            return redirect()->back()->with('error',config('app.NEGATIVE_MESSAGE'));
        }
        if ($request->hasFile('images')){
            $ms=new MediaService();
            foreach ($request->images as $image){
                $ms->upload($image,'cars','CAR_IMAGES',$car->id);
            }
        }
        return redirect()->route('car.list.view')->with('success',config('app.POSITIVE_MESSAGE'));
    }

    function import(CarRequest $request)
    {
        try{
            Excel::import(new CarImport, $request->excel_file);
            $arrEntries=explode('|',session('SES_CAR_STORE'));
            for ($i = 0; $i < count($arrEntries)-1; $i++) {
                $temp=explode("<!>", $arrEntries[$i]);
                $car=Car::where(['slug' => $temp])->first();
                $media=Media::where(['id'=>$temp[1]])->update(['refrence_id' => $car->id]);
            }
            session()->forget(config('app.SES_CAR_STORE'));
            return redirect()->back()->with('success', 'Imported Successfully!');
        } catch (\Exception $exception){
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
    function listView(Request $request)
    {
        $ps=new PitService(Car::class);
        $cars=$ps->index($request);
        return view('car.index',['cars'=>$cars]);
    }
    function delete(Request $request)
    {
        $ps=new PitService(Car::class);
        $cars=$ps->delete($request->id);
        return view('car.index',['cars'=>$cars]);
    }

    function createCarFeatureView()
    {
        return view('car.feature.create');
    }
    function createCarFeature(CarRequest $request)
    {
        $r=new Extra();
        $r->identifier=config('app.CAR_FEATURE');
        $r->value=$request->title;
        $r->created_by=auth()->user()->id;
        if (!$r->save()){
            return redirect()->back()->with('error',config('app.NEGATIVE_MESSAGE'));
        }
        return redirect()->route('car.feature.list.view')->with('success',config('app.POSITIVE_MESSAGE'));
    }
    function listCarFeature(Request $request)
    {
        $ps=new PitService(Extra::class);
        $features=$ps->index($request,['identifier'=>'CAR_FEATURE','id'=>6]);
        return view('car.feature.index',['features'=>$features]);
    }
}
