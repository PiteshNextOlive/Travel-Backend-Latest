<?php

namespace App\Http\Controllers;

use App\Http\Services\MediaService;
use App\Http\Services\PitService;
use App\Imports\BoatImport;
use App\Imports\CarImport;
use App\Models\Boat;
use App\Models\Location;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BoatController extends Controller
{
    function createView()
    {
        $locations=Location::all();
        return view('boat.create',['locations'=>$locations]);
    }
    function apiIndex(Request $request)
    {
        $cs = new PitService(Boat::class);
        $d = $cs->index($request);
        return respond((bool)$d,null,$d);
    }
    function create(Request $request)
    {
        $validation=$request->validate([
            'title'=>'required',
            'content'=>'required',
            'type'=>'required|in:yatch,boat',
            'location'=>'required|exists:locations,id',
            'address'=>'required',

            'base_price_daily'=>'required|numeric',
            'price_mod_type_daily'=>'in:p,f',
            'price_mod_amount_daily'=>'required|numeric',

            'base_price_weekly'=>'required|numeric',
            'price_mod_type_weekly'=>'in:p,f',
            'price_mod_amount_weekly'=>'required|numeric',

            'base_price_monthly'=>'required|numeric',
            'price_mod_type_monthly'=>'in:p,f',
            'price_mod_amount_monthly'=>'required|numeric',

        ]);

        $faqjson=$specificationJson=[];
        for ($i = 0; $i < count($request->faq_q); $i++) {
            $faqjson[$i][$request->faq_q[$i]]=$request->faq_a[$i];
        }
        for ($i = 0; $i < count($request->specification_k); $i++) {
            $specificationJson[$i][$request->specification_k[$i]]=$request->specification_v[$i];
        }
        $faqjson=json_encode($faqjson);
        $specificationJson=json_encode($specificationJson);

        $boat=new Boat();
        $boat->title=$request->title;
        $boat->slug=\Str::slug($request->title);
        $boat->content=$request->post('content');
        $boat->location=$request->location;
        $boat->address=$request->address;
        $boat->youtube_link=$request->youtube_link;
        $boat->vin=$request->vin;
        $boat->latitude=$request->latitude;
        $boat->longitude=$request->longitude;
        $boat->base_price_daily=$request->base_price_daily;
        $boat->price_mod_sign_daily='-';
        $boat->price_mod_type_daily=$request->price_mod_type_daily;
        $boat->price_mod_amount_daily=$request->price_mod_amount_daily;

        $boat->base_price_weekly=$request->base_price_weekly;
        $boat->price_mod_sign_weekly='-';
        $boat->price_mod_type_weekly=$request->price_mod_type_weekly;
        $boat->price_mod_amount_weekly=$request->price_mod_amount_weekly;

        $boat->base_price_monthly=$request->base_price_monthly;
        $boat->price_mod_sign_monthly='-';
        $boat->price_mod_type_monthly=$request->price_mod_type_monthly;
        $boat->price_mod_amount_monthly=$request->price_mod_amount_monthly;

        $boat->faq=$faqjson;
        $boat->specification=$specificationJson;
//        return $request->hasFile('images');
        if (!$boat->save()){
            return redirect()->back()->with('error',config('app.NEGATIVE_MESSAGE'));
        }

        if ($request->hasFile('images')){
            $ms=new MediaService();
            foreach ($request->images as $image){
                $ms->upload($image,'boats','BOAT_IMAGES',$boat->id);
            }
        }
        return redirect()->route('boat.list.view')->with('success',config('app.POSITIVE_MESSAGE'));
    }
    function import(Request $request)
    {
        try{
            Excel::import(new BoatImport, $request->excel_file);
            return redirect()->back()->with('success', 'Imported Successfully!');
        } catch (\Exception $exception){
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
    function listView(Request $request)
    {
        $ps=new PitService(Boat::class);
        $boats=$ps->index($request);
        return view('boat.index',['boats'=>$boats]);
    }
    function delete(Request $request)
    {
        $ps=new PitService(Boat::class);
        $boats=$ps->delete($request->id);
        return view('boat.index',['boats'=>$boats]);
    }

//    function createBoatFeatureView()
//    {
//        return view('boat.feature.create');
//    }
//    function createBoatFeature(BoatRequest $request)
//    {
//        $r=new Extra();
//        $r->identifier=config('app.CAR_FEATURE');
//        $r->value=$request->title;
//        $r->created_by=auth()->user()->id;
//        if (!$r->save()){
//            return redirect()->back()->with('error',config('app.NEGATIVE_MESSAGE'));
//        }
//        return redirect()->route('boat.feature.list.view')->with('success',config('app.POSITIVE_MESSAGE'));
//    }
//    function listBoatFeature()
//    {
//        return view('boat.feature.index');
//    }
}
