<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoactionRequest;
use App\Http\Services\PitService;
use App\Models\City;
use App\Models\Country;
use App\Models\Location;
use App\Models\State;
use Illuminate\Http\Request;

class LocationController extends Controller
{
//    function createView()
//    {
//        return view('location.create');
//    }
//    function create(LoactionRequest $request)
//    {
//        $r=Location::create($request->post());
//        if (!$r){
//            return redirect()->back()->with('error','Unable to perform this action');
//        } else{
//            return redirect()->route('location.list.view')->with('success','Action performed successfully');
//        }
//    }
//    function listView(Request $request)
//    {
//        $ps=new PitService(Location::class);
//        $locations=$ps->index($request);
//        return view('location.index',['locations'=>$locations]);
//    }
    function listCountry(Request $request)
    {
        $ps=new PitService(Country::class);
        $returnable=$ps->index($request);
        if (in_array('api',$request->route()->middleware())){
            return respond((bool)$returnable,null,$returnable);
        }
        if (in_array('web',$request->route()->middleware())){

        }
    }
    function listState(Request $request)
    {
        $ps=new PitService(State::class);
        $returnable=$ps->index($request);
        if (in_array('api',$request->route()->middleware())){
            return respond((bool)$returnable,null,$returnable);
        }
        if (in_array('web',$request->route()->middleware())){

        }
    }
    function listCity(Request $request)
    {
        $ps=new PitService(City::class);
        $returnable=$ps->index($request);
        if (in_array('api',$request->route()->middleware())){
            return respond((bool)$returnable,null,$returnable);
        }
        if (in_array('web',$request->route()->middleware())){
            return view('index',['location'=>$returnable]);
        }
    }



}
