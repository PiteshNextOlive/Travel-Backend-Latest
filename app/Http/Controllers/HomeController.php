<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Services\PitService;
use App\Models\Airport;
use App\Models\getaroom_property;
use App\Models\User;
use Auth;
use Http;
use Illuminate\Http\Request;
use Route;
use Spatie\Permission\Models\Role;

class HomeController extends Controller
{
    public function test()
    {
        $file = \File::get(public_path('uploads/json/airports.json'));
        $data=json_decode(\File::get(public_path('uploads/json/airports.json')));
        foreach ($data as $d){
            Airport::insert((array)$d);
        }
//        $xml = simplexml_load_string(\File::get(public_path('properties.xml')));
//        $i=0;
//        foreach ($xml as $key => $value){
//            $value=(array)$value;
//            getaroom_property::create((array)json_decode(json_encode($value)));
//            if ($i==1542){
////                echo gettype($value['location-state']);
//            }
//            $i++;
//        }
    }
    function getARoomApifetchPost()
    {
        $response = Http::withOptions(['verify' => false,'timeout' => 3600])->get("https://book.integration2.testaroom.com/api/properties.xml?api_key=acb07ecd-d9f0-525d-a04f-04e33e541ae3&auth_token=35ef60ad-8062-553f-b0be-72f5fd0ee672&room_images=true");
        // Check if the request was successful (status code 200)
        if ($response->successful()) {
            $fileContent = $response->body();
            \File::put('get_a_room.xml',$fileContent);
            echo "Successfully";
        } else {
            abort($response->status());
        }
    }

    function getARoomSeed()
    {
        if ( \File::exists(public_path("get_a_room.xml"))){
        $xml = simplexml_load_string(\File::get(public_path('get_a_room.xml')));
        $i=0;
        foreach ($xml as $key => $value){
            $value=(array)$value;
            getaroom_property::create((array)json_decode(json_encode($value)));
//            if ($i==1542){
//                echo gettype($value['location-state']);
//            }
            $i++;
        }
        } else{
            echo "Base file not available at- ".public_path("get_a_room.xml");
        }

    }
    public function index_view()
    {
        return view('index');
    }
    public function login_view()
    {
        return view('auth.login');
    }
    public function login(UserRequest $request)
    {
        if (Auth::attempt($request->only('email','password'))) {
            $request->session()->regenerate();
            return redirect()->route('dashboard.view');
        } else{
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ])->onlyInput('email');
        }
    }
    public function signup_view()
    {
        return view('auth.signup');
    }
    public function signup_store(UserRequest $request)
    {
        $ps=new PitService(User::class);
        if ($u=User::find($ps->create($request)->id)){
            $u->assignRole($request->type);
            auth()->loginUsingId($u->id);
            return redirect()->route('login')->with('success','Signed up successfully! Please login')->withInput();
        }
    }
    public function logout(Request $request)
    {
        auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login.view');
    }
}
