<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    function login(UserRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            return respond(true,"The provided credentials are incorrect.");
        }
        $user['token']=$user->createToken($request->ip(), ['*'], now()->addDay(365))->plainTextToken;
        return respond(true,"Login successful",$user);
    }
    function logout(Request $request)
    {
        $stts=auth()->user()->tokens()->delete();
        return respond($stts);
    }
}
