<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Http\Requests\FlyEasyRequest;
use Illuminate\Support\Facades\Http;

class FlyEasyController extends Controller
{
    function emptyLegSearch(FlyEasyRequest $request)
    {
        $request->merge(['apiKey'=>config('app.FLYEASY_KEY')]);
        $response = Http::withHeaders([
            'Accept' => 'application/json',
        ])->withoutVerifying()
            ->post(config('app.FLYEASY_ENDPOINT').'emptylegs/search',$request->post());

        return $response->json();

    }
}
