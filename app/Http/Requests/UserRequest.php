<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Route;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =[];
        switch (Route::currentRouteName()){
            case 'signup.store':
                $rules= $this->create_rules();
                break;
            case 'api.user.login'||'login.store':
                $rules= $this->login_rules();
                break;
        }
        return $rules;
    }
    function create_rules()
    {
        return [
            'type'=>'required|exists:roles,name',
            'name'=>'required',
            'email'=>'required|unique:users,email|email',
            'password'=>'confirmed',
        ];
    }

    private function login_rules()
    {

        return [
            'email'=>'required|exists:users,email',
            'password'=>'required',
        ];
    }
}
