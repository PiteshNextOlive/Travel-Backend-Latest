<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Route;

class FlyEasyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =null;
        switch (Route::currentRouteName()){
            case 'api.flyeasy.emptylegs.search':
                $rules= $this->empty_leg_search_rules();
                break;
        }
        return $rules;
    }

    public function messages(): array
    {
        return [
            'fromICAO.required'=>'"fromICAO" is required',
            'toICAO'=>'"toICAO" is required',
        ];
    }
    function empty_leg_search_rules()
    {
        return [
            'fromICAO'=>'required',
            'toICAO'=>'required',
        ];
    }

}
