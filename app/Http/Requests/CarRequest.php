<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Route;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =null;
        switch (Route::currentRouteName()){
            case 'car.create':
                $rules= $this->create_rule();
                break;
            case 'car.feature.create':
                $rules= $this->create_feature_rule();
                break;
            case 'car.import':
                $rules= $this->create_import_rule();
                break;
        }
        return $rules;
    }

    private function create_rule(): array
    {
        return [
            'tkn'=>'required'
        ];
    }
    private function create_feature_rule(): array
    {
        return [
            'title'=>'required'
        ];
    }
    private function create_import_rule(): array
    {
        return [
            'excel_file'=>'required|file'
        ];
    }
}
