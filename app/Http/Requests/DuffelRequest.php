<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Route;

class DuffelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }
    public function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =null;
        switch (Route::currentRouteName()){
            case 'api.duffel.offer_request':
                $rules= $this->rule_offer_request();
                break;
        }
        return $rules;
    }

    public function messages(): array
    {
        return [
            'id.required'=>'id is required',
        ];
    }

    private function rule_offer_request(): array
    {
        return [
            'id'=>'required'
        ];
    }
}
