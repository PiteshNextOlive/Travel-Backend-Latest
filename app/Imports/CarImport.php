<?php

namespace App\Imports;

use App\Http\Services\MediaService;
use App\Http\Services\PitService;
use App\Models\Car;
use App\Models\Extra;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;

class CarImport implements ToModel, WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $import= new Car([
            'title'     => $row[0],
            'slug'     => \Str::slug($row[0]),
            'content'     => $row[1],
            'address'     => $row[2],
            'youtube_link'     => $row[3],
            'vin'     => $row[4],
            'latitude'     => $row[5],
            'longitude'     => $row[6],
            'base_price_daily'     => $row[7],
            'price_mod_sign_daily'     => '-',
            'price_mod_type_daily'     => $row[8],
            'price_mod_amount_daily'     => $row[9],
            'base_price_weekly'     => $row[10],
            'price_mod_sign_weekly'     => '-',
            'price_mod_type_weekly'     => $row[11],
            'price_mod_amount_weekly'     => $row[12],
            'base_price_monthly'     => $row[13],
            'price_mod_sign_monthly'     => '-',
            'price_mod_type_monthly'     => $row[14],
            'price_mod_amount_monthly'     => $row[15],
            'gear_type'     => $row[16],
            'door'     => $row[17],
        ]);
        $this->imageDumper($row);
        return $import;
    }
    public function rules(): array
    {
        return [
            '8' => Rule::in(['p','f']),
            '11' => Rule::in(['p','f']),
            '14' => Rule::in(['p','f']),
        ];
    }

    public function customValidationMessages()
    {
        return [
            '8.in' => 'price_mod_type_daily should be either p or f',
            '11.in' => 'price_mod_type_weekly should be either p or f',
            '14.in' => 'price_mod_type_monthly should be either p or f',
        ];
    }

    function imageDumper($row)
    {
        $strImages=$row[18];
        $arrImages=array_filter(explode("\n",$strImages));
        Log::channel("pitesh")->debug("Count of images: ".collect($arrImages));
        if (count($arrImages)){
            $ps=new PitService(null);
            $ms=new MediaService();
            for ($i = 0; $i < count($arrImages); $i++) {
                $contnt=$ps->fetchGet([],$arrImages[$i],[]);
                $curFileName='cars/imported/'.basename(parse_url($arrImages[$i], PHP_URL_PATH))??uniqid().".jpg";
                $m=$ms->write($contnt,$curFileName,'CAR_IMAGES',null,null,auth()->user()->id);
//                session([config('app.SES_CAR_STORE')=>session(config('app.SES_CAR_STORE')).','.json_encode([\Str::slug($row[0])=>$curFileName])]);
                Extra::create([
                    'identifier' => 'PENDING_CAR_UPLOAD',
                    'value' => \Str::slug($row[0]),
                    'additional' => $m->id,
                    'created_by' => auth()->user()->id
                ]);
            }
        } else{
            Log::channel("pitesh")->debug("No images real images array could be formed");
        }
    }
}
