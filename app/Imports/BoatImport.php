<?php

namespace App\Imports;

use App\Models\Boat;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;

class BoatImport implements ToModel, WithValidation
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Boat([
            'title'     => $row[0],
            'slug'     => \Str::slug($row[0]),
            'content'     => $row[1],
            'address'     => $row[2],
            'youtube_link'     => $row[3],
            'vin'     => $row[4],
            'latitude'     => $row[5],
            'longitude'     => $row[6],
            'base_price_daily'     => $row[7],
            'price_mod_sign_daily'     => '-',
            'price_mod_type_daily'     => $row[8],
            'price_mod_amount_daily'     => $row[9],
            'base_price_weekly'     => $row[10],
            'price_mod_sign_weekly'     => '-',
            'price_mod_type_weekly'     => $row[11],
            'price_mod_amount_weekly'     => $row[12],
            'base_price_monthly'     => $row[13],
            'price_mod_sign_monthly'     => '-',
            'price_mod_type_monthly'     => $row[14],
            'price_mod_amount_monthly'     => $row[15],
            'type'     => $row[16],
            'images' => $row[17]
        ]);
    }
    public function rules(): array
    {
        return [
            '8' => Rule::in(['p','f']),
            '11' => Rule::in(['p','f']),
            '14' => Rule::in(['p','f']),
            '16' => Rule::in(['yatch','boat']),
        ];
    }

    public function customValidationMessages()
    {
        return [
            '8.in' => 'price_mod_type_daily should be either p or f',
            '11.in' => 'price_mod_type_weekly should be either p or f',
            '14.in' => 'price_mod_type_monthly should be either p or f',
            '16.in' => 'type should be either yatch or boat',
        ];
    }
}
