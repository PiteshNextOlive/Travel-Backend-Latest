const pitLib = {
    test:function () {
        alert('ok')
    },
    util:{
        uniquid:function () {
            const timestamp = new Date().getTime().toString(16); // Convert current timestamp to hexadecimal
            const randomString = Math.random().toString(36).substr(2, 5); // Generate a random string
            return timestamp + randomString; // Combine timestamp and random string
        }
    },
    pitFetch:function (method="POST",url,formData,target=null) {
        var preHtml
        if (target){
            preHtml=$(target).html();
            $(target).addClass('disabled').html(`<i class='fas ms-2 fa-spinner fa-pulse fa-lg bold'></i>`)
        }
        $.ajaxSetup({headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }});
        return $.ajax({type: method, url: url, data: formData,
            beforeSend: function () {;},
            success: function (data) {;},
            complete: function (data) {
                if (target){
                    $(target).removeClass('disabled').html(preHtml)
                }
            }
        });
    },
    pitFetchForm:function (method="GET",url,formData) {
        $.ajaxSetup({headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }});
        return $.ajax({type: method, url: url, data: formData,cache: false,
            contentType: false, // Set to false when using FormData
            processData: false,
            beforeSend : function(){;},
            success: function (data) {;},
            complete:function(){;}
        });
    },
}

