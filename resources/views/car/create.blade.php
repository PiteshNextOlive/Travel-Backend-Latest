@extends('layout.panel')
@section('breadcrumb')
    <li class="breadcrumb-item pe-3"><a href="{{route('car.list.view')}}" class="pe-3">Cars</a></li>
    <li class="breadcrumb-item pe-3 text-muted">Create</li>
@endsection
@section('content')
    <!--begin::Post-->
    <div class="row">
        <div class="col">
            <div class="card card-custom">
                <form method="post" action="{{route('car.create')}}" enctype="multipart/form-data" onsubmit="return formValidator">
                    @csrf
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-chat-1 text-primary"></i>
                            </span>
                            <h3 class="card-label">
                                Create Car
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a  data-bs-toggle="modal" data-bs-target="#kt_modal_1" class="btn btn-sm btn-light-primary font-weight-bold">
                                <i class="flaticon2-cube"></i> Excel Import
                            </a>
                        </div>
                    </div>
                    <div class="card-body">

                        @if($errors->any()||session('error'))
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <ul>
                                            @if($errors->any())
                                                @foreach($errors->all() as $e)
                                                    <li>{{$e}}</li>
                                                @endforeach
                                            @endif
                                            @if(session('error'))
                                                <li>{{session('error')}}</li>
                                            @endif
                                        </ul>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(session('success'))
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {{session('success')}}
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input value="{{old('title')}}" id="title" name="title" class="form-control"  placeholder="Enter Car Title"/>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <label>Content</label>
                                <textarea name="content" class="editor form-control">{{old('content')}}</textarea>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-8">
                                <div class="form-group">
                                    <label for="youtube_link">Youtube Link</label>
                                    <input value="{{old('youtube_link')}}" id="youtube_link" name="youtube_link" class="form-control"  placeholder="Youtube Link"/>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-8">
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <label for="youtube_link">Faq</label>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-primary btn-sm" id="btn_add_faq">Add</button>
                                        </div>
                                    </div>
                                    <div class="card p-3">
                                        <table id="tbl_faq_input" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Data</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-8">
                                <div class="form-group">
                                    <div class="row align-items-center">
                                        <div class="col">
                                            <label for="youtube_link">Specification</label>
                                        </div>
                                        <div class="col">
                                            <button type="button" class="btn btn-primary btn-sm" id="btn_add_specification">Add</button>
                                        </div>
                                    </div>
                                    <div class="card p-3">
                                        <table id="tbl_specification_input" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Data</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <div class="form-group">
                                    <label for="banner_image">Images</label>
                                    <input id="banner_image" name="images[]" class="form-control" type="file" accept="image/*" multiple/>
                                </div>
                            </div>
                            <div class="col">
                                <label for="features">Features</label>
                                <input id="features" name="features" class="form-control"/>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-4">
                                <label for="address">Address</label>
                                <input value="{{old('address')}}" id="address" name="address" class="form-control" placeholder="Address"/>
                            </div>
                            <div class="col-2">
                                <label for="latitude">Lattitude</label>
                                <input value="{{old('latitude')}}" id="latitude" name="latitude" type="number" class="form-control" placeholder="Latitude"/>
                            </div>
                            <div class="col-2">
                                <label for="longitude">Longitude</label>
                                <input value="{{old('longitude')}}" id="longitude" name="longitude" type="number"  class="form-control" placeholder="Longitude"/>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <select id="location" name="location" class="form-control">
                                        <option disabled selected>--Select--</option>
                                        @foreach($locations as $location)
                                            <option @selected($location->id===old('location')) value="{{$location->id}}">{{$location->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-4">
                                <label for="vin">VIN</label>
                                <input value="{{old('vin')}}" id="vin" name="vin" class="form-control" placeholder="VIN"/>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="gear_type">Gear Type</label>
                                    <select id="gear_type" name="gear_type" class="form-control">
                                        <option disabled selected>--Select--</option>
                                        <option @selected(old('gear_type')==='automatic') value="automatic">Automatic</option>
                                        <option @selected(old('gear_type')==='manual') value="manual">>Manual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <label for="door">Doors</label>
                                <input value="{{old('door')}}" id="door" name="door" class="form-control" type="number" min="2" placeholder="Doors"/>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <label>Pricing</label>
                                <table class="table table-responsive table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="bg-light-primary">Timeframe</th>
                                            <th>Base Price</th>
                                            <th>Discount Type</th>
                                            <th>Discount Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="bg-light-primary">
                                                Daily
                                            </td>
                                            <td>
                                                <input value="{{old('base_price_daily')}}" class='form-control' type="number" name="base_price_daily" placeholder="Base Price">
                                            </td>
                                            <td>
                                                <select name="price_mod_type_daily" class="form-control">
                                                    <option disabled selected>--Select--</option>
                                                    <option @selected(old('price_mod_type_daily')==='f') value="f">Flat</option>
                                                    <option @selected(old('price_mod_type_daily')==='p') value="p">Percent</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input value="{{old('price_mod_amount_daily')}}" class='form-control' type="number" name="price_mod_amount_daily" placeholder="Amount">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bg-light-primary">
                                                Weekly
                                            </td>
                                            <td>
                                                <input value="{{old('base_price_weekly')}}" class='form-control' type="number" name="base_price_weekly" placeholder="Base Price">
                                            </td>
                                            <td>
                                                <select name="price_mod_type_weekly" class="form-control">
                                                    <option disabled selected>--Select--</option>
                                                    <option @selected(old('price_mod_type_weekly')==='f') value="f">Flat</option>
                                                    <option @selected(old('price_mod_type_weekly')==='p') value="p">Percent</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input value="{{old('price_mod_amount_weekly')}}" class='form-control' type="number" name="price_mod_amount_weekly" placeholder="Amount">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bg-light-primary">
                                                Monthly
                                            </td>
                                            <td>
                                                <input value="{{old('base_price_monthly')}}" class='form-control' type="number" name="base_price_monthly" placeholder="Base Price">
                                            </td>
                                            <td>
                                                <select name="price_mod_type_monthly" class="form-control">
                                                    <option disabled selected>--Select--</option>
                                                    <option @selected(old('price_mod_type_monthly')==='f') value="f">Flat</option>
                                                    <option @selected(old('price_mod_type_monthly')==='p') value="p">Percent</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input value="{{old('price_mod_amount_monthly')}}" class='form-control' type="number" name="price_mod_amount_monthly" placeholder="Amount">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <span></span>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="kt_modal_1">
        <form method="post" action="{{route('car.import')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="card-title">
                                <span class="card-icon">
                                    <i class="flaticon2-chat-1 text-primary"></i>
                                </span>
                            <h3 class="card-label">
                                Excel Import
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a target="_blank" href="/public/uploads/excel/car_excel_import.xlsx" class="btn btn-sm btn-success font-weight-bold">
                                <i class="flaticon2-cube"></i> Demo File
                            </a>
                        </div>
                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-2x"></span>
                        </div>
                        <!--end::Close-->
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-8">
                                <label for="excel_file">Choose Excel File</label>
                                <input accept=".xlsx,.xls" id="excel_file" name="excel_file" type="file" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-lig    ht" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!--end::Post-->
@endsection
@section('page_script')
    <script src="/public/panel/ckeditor/build/ckeditor.js"></script>
    <script>
        function formValidator() {
            return false
        }
        function getFaqRow() {
            curId=pitLib.util.uniquid()
            return `
        <tr id='${curId}'>
            <td class="col-4">
                <input class='form-control' type="text" name="faq_q[]" placeholder="Ex. Can I bring my pet?">
            </td>
            <td class="col">
                <input class='form-control' type="text" name="faq_a[]" placeholder="Yes Pets are allowed">
            </td>
            <td>
                <button onclick="delFaqRow('${curId}')" type="button" class="btn btn-danger btn-sm rounded btn_del_faq_row">Delete</button>
            </td>
         </tr>
        `
        }
        function getSpecificationRow() {
            curId=pitLib.util.uniquid()
            return `
        <tr id='${curId}'>
            <td class="col-4">
                <input class='form-control' type="text" name="specification_k[]" placeholder="Ex. Max Speed">
            </td>
            <td class="col">
                <input class='form-control' type="text" name="specification_v[]" placeholder="240 Km/Hour">
            </td>
            <td>
                <button onclick="delSpecifRow('${curId}')" type="button" class="btn btn-danger btn-sm rounded btn_del_specification_row">Delete</button>
            </td>
         </tr>
        `
        }
        $( document ).ready(function() {
            $("#tbl_faq_input").find("tbody").html(getFaqRow())
            $("#tbl_specification_input").find("tbody").html(getSpecificationRow())
            var tagify = new Tagify($("#features")[0], {
                enforceWhitelist: true,
                whitelist:{!! $features !!},
                dropdown: {
                    maxItems: 20,
                    classname: "tags-look",
                    enabled: 0,
                    closeOnSelect: false
                }
            })
        });
        ClassicEditor
            .create( document.querySelector( '.editor' ), {
                // Editor configuration.
            } )
            .then( editor => {
                window.editor = editor;
            } )
        $("#btn_add_faq").click(function () {
            $("#tbl_faq_input").find("tbody").append(getFaqRow())
        })
        function delFaqRow(id) {
            $("#tbl_faq_input").find("#"+id).fadeOut(500,function () {
                $("#tbl_faq_input").find("#"+id).remove()
            })
        }

        $("#btn_add_specification").click(function () {
            $("#tbl_specification_input").find("tbody").append(getSpecificationRow())
        })
        function delSpecifRow(id) {
            $("#tbl_specification_input").find("#"+id).fadeOut(500,function () {
                $("#tbl_specification_input").find("#"+id).remove()
            })
        }
    </script>
@endsection
