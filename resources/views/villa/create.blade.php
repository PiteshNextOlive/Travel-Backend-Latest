@extends('layout.panel')
@section('breadcrumb')
    <li class="breadcrumb-item pe-3"><a href="{{route('villa.list.view')}}" class="pe-3">Villas</a></li>
    <li class="breadcrumb-item pe-3">Create</li>
@endsection
@section('content')
    <!--begin::Post-->
    <div class="row">
        <div class="col">
            <div class="card card-custom">
                <form method="post" action="{{route('villa.create')}}" enctype="multipart/form-data" onsubmit="return formValidator">
                    @csrf
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-chat-1 text-primary"></i>
                            </span>
                            <h3 class="card-label">
                                Create Villa
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a  data-bs-toggle="modal" data-bs-target="#kt_modal_1" class="btn btn-sm btn-light-primary font-weight-bold">
                                <i class="flaticon2-cube"></i> Excel Import
                            </a>
                        </div>
                    </div>
                    <div class="card-body">

                        @if($errors->any()||session('error'))
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <ul>
                                            @if($errors->any())
                                                @foreach($errors->all() as $e)
                                                    <li>{{$e}}</li>
                                                @endforeach
                                            @endif
                                            @if(session('error'))
                                                <li>{{session('error')}}</li>
                                            @endif
                                        </ul>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                        @endif
                            @if(session('success'))
                                <div class="row mt-4">
                                    <div class="col">
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            {{session('success')}}
                                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input value="{{old('name')}}" id="name" name="name" class="form-control"  placeholder="Enter Villa Name"/>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-6">
                                <label for="description">Details</label>
                                <textarea id="description" name="description" class="editor form-control">{{old('description')}}</textarea>
                            </div>
                            <div class="col-6">
                                <label for=houseRules"">House Rules</label>
                                <textarea id="houseRules" name="houseRules" class="editor form-control">{{old('houseRules')}}</textarea>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <label for="country">Country</label>
                                <input id="country" name="country" class="form-control" max="1"/>
                            </div>
                           <div class="col">
                                <label for="state">State</label>
                                <select id="state" name="state" class="form-control">
                                    <option disabled selected>--Select--</option>
                                </select>
                           </div>
                           <div class="col">
                                <label for="city">City</label>
                                <select id="city" name="city" class="form-control">
                                    <option disabled selected>--Select--</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="listingImages">Images</label>
                                    <input id="listingImages" name="listingImages[]" class="form-control" type="file" accept="image/*" multiple/>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-4">
                                <label for="address">Address</label>
                                <input value="{{old('address')}}" id="address" name="address" class="form-control" placeholder="Address"/>
                            </div>
                            <div class="col-2">
                                <label for="latitude">Lattitude</label>
                                <input value="{{old('lat')}}" id="lat" name="lat" type="text" class="form-control" placeholder="Latitude"/>
                            </div>
                            <div class="col-2">
                                <label for="longitude">Longitude</label>
                                <input value="{{old('lng')}}" id="lng" name="lng" type="text"  class="form-control" placeholder="Longitude"/>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="zipcode">Zip Code</label>
                                    <input value="{{old('zipcode')}}" id="zipcode" name="zipcode" type="number" class="form-control" placeholder="Zip Code"/>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-4">
                                <label for="price">Price</label>
                                <input value="{{old('price')}}" id="price" name="price" class="form-control" placeholder="Price"/>
                            </div>
                            <div class="col-2">
                                <label for="starRating">Star Rating</label>
                                <input value="{{old('starRating')}}" id="starRating" name="starRating" type="number" class="form-control" placeholder="Star Rating"/>
                            </div>
                            <div class="col-2">
                                <label for="weeklyDiscount">weekly Discount</label>
                                <input value="{{old('weeklyDiscount')}}" id="weeklyDiscount" name="weeklyDiscount" type="number"  class="form-control" placeholder="Weekly Discount"/>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="monthlyDiscount">Monthly Discount</label>
                                    <input value="{{old('monthlyDiscount')}}" id="monthlyDiscount" name="monthlyDiscount" type="number" class="form-control" placeholder="Monthly Discount"/>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-8">
                                <label for="listingAmenities">Listing Amenities</label>
                                <input value="" id="listingAmenities" name="listingAmenities" type="text"  class="form-control" placeholder="Listing Amenities"/>
                            </div>

                            {{-- <div class="col">
                                <div class="form-group">
                                    <label for="monthlyDiscount">Monthly Discount</label>
                                    <input value="{{old('monthlyDiscount')}}" id="monthlyDiscount" name="monthlyDiscount" type="number" class="form-control" placeholder="Monthly Discount"/>
                                </div>
                            </div> --}}
                        </div>

                        {{-- <div class="row mt-4">
                            <div class="col-4">
                                <label for="vin">VIN</label>
                                <input value="{{old('vin')}}" id="vin" name="vin" class="form-control" placeholder="VIN"/>
                            </div>
                            <div class="col-4">

                            </div>
                            <div class="col-4">
                                <label for="type">Type</label>
                                <select id="type" name="type" class="form-control">
                                    <option disabled selected>--Select--</option>
                                    <option @selected(old('type')==='yatch') value="yatch">Yatch</option>
                                    <option @selected(old('type')==='boat') value="boat">Boat</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col">
                                <label>Pricing</label>
                                <table class="table table-responsive table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="bg-light-primary">Timeframe</th>
                                            <th>Base Price</th>
                                            <th>Discount Type</th>
                                            <th>Discount Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="bg-light-primary">
                                                Daily
                                            </td>
                                            <td>
                                                <input value="{{old('base_price_daily')}}" class='form-control' type="number" name="base_price_daily" placeholder="Base Price">
                                            </td>
                                            <td>
                                                <select name="price_mod_type_daily" class="form-control">
                                                    <option disabled selected>--Select--</option>
                                                    <option @selected(old('price_mod_type_daily')==='f') value="f">Flat</option>
                                                    <option @selected(old('price_mod_type_daily')==='p') value="p">Percent</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input value="{{old('price_mod_amount_daily')}}" class='form-control' type="number" name="price_mod_amount_daily" placeholder="Amount">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bg-light-primary">
                                                Weekly
                                            </td>
                                            <td>
                                                <input value="{{old('base_price_weekly')}}" class='form-control' type="number" name="base_price_weekly" placeholder="Base Price">
                                            </td>
                                            <td>
                                                <select name="price_mod_type_weekly" class="form-control">
                                                    <option disabled selected>--Select--</option>
                                                    <option @selected(old('price_mod_type_weekly')==='f') value="f">Flat</option>
                                                    <option @selected(old('price_mod_type_weekly')==='p') value="p">Percent</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input value="{{old('price_mod_amount_weekly')}}" class='form-control' type="number" name="price_mod_amount_weekly" placeholder="Amount">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bg-light-primary">
                                                Monthly
                                            </td>
                                            <td>
                                                <input value="{{old('base_price_monthly')}}" class='form-control' type="number" name="base_price_monthly" placeholder="Base Price">
                                            </td>
                                            <td>
                                                <select name="price_mod_type_monthly" class="form-control">
                                                    <option disabled selected>--Select--</option>
                                                    <option @selected(old('price_mod_type_monthly')==='f') value="f">Flat</option>
                                                    <option @selected(old('price_mod_type_monthly')==='p') value="p">Percent</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input value="{{old('price_mod_amount_monthly')}}" class='form-control' type="number" name="price_mod_amount_monthly" placeholder="Amount">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <span></span>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" id="kt_modal_1">
        <form method="post" action="{{route('boat.import')}}" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="card-title">
                                <span class="card-icon">
                                    <i class="flaticon2-chat-1 text-primary"></i>
                                </span>
                            <h3 class="card-label">
                                Excel Import
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a target="_blank" href="/public/uploads/excel/car_excel_import.xlsx" class="btn btn-sm btn-success font-weight-bold">
                                <i class="flaticon2-cube"></i> Demo File
                            </a>
                        </div>
                        <!--begin::Close-->
                        <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                            <span class="svg-icon svg-icon-2x"></span>
                        </div>
                        <!--end::Close-->
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-8">
                                <label for="excel_file">Choose Excel File</label>
                                <input accept=".xlsx,.xls" id="excel_file" name="excel_file" type="file" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-lig    ht" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!--end::Post-->
@endsection
@section('page_script')
    <script src="/public/panel/ckeditor/build/ckeditor.js"></script>
    <script>
        function formValidator() {
            return false
        }
        function getFaqRow() {
            curId=pitLib.util.uniquid()
            return `
        <tr id='${curId}'>
            <td class="col-4">
                <input class='form-control' type="text" name="faq_q[]" placeholder="Ex. Is this boat registered?">
            </td>
            <td class="col">
                <input class='form-control' type="text" name="faq_a[]" placeholder="Yes all boats are registered.">
            </td>
            <td>
                <button onclick="delFaqRow('${curId}')" type="button" class="btn btn-danger btn-sm rounded btn_del_faq_row">Delete</button>
            </td>
         </tr>
        `
        }
        function getSpecificationRow() {
            curId=pitLib.util.uniquid()
            return `
        <tr id='${curId}'>
            <td class="col-4">
                <input class='form-control' type="text" name="specification_k[]" placeholder="Ex. GPS Navigation">
            </td>
            <td class="col">
                <input class='form-control' type="text" name="specification_v[]" placeholder="Yes">
            </td>
            <td>
                <button onclick="delSpecifRow('${curId}')" type="button" class="btn btn-danger btn-sm rounded btn_del_specification_row">Delete</button>
            </td>
         </tr>
        `
        }
        let field_country=$("#country")
        $( document ).ready(function() {
            $("#tbl_faq_input").find("tbody").html(getFaqRow())
            $("#tbl_specification_input").find("tbody").html(getSpecificationRow())
            elemCountry=new Tagify(field_country[0], {
                enforceWhitelist: true,
                whitelist:{!! $countries !!},
                maxTags: 1,
                dropdown: {
                    maxItems: 300,
                    classname: "tags-look",
                    enabled: 0,
                    closeOnSelect: false
                }
            })
            // elemState=new Tagify(field_state[0], {
            //     enforceWhitelist: true,
            //     whitelist:{!! $countries !!},
            //     maxTags: 1,
            //     dropdown: {
            //         maxItems: 300,
            //         classname: "tags-look",
            //         enabled: 0,
            //         closeOnSelect: false
            //     }
            // })
            elemCountry.on('add', onAddCountry)

            var amityTags = document.querySelector('input[name=listingAmenities]');
            // initialize Tagify on the above input node reference
            new Tagify(amityTags)

            function onAddCountry(e) {
                $("#city").html('<option disabled selected>--Select--</option>')
                pitLib.pitFetch("POST","{{route('api.location.state.list')}}?paginate=500",{country_id:e.detail.data.id}).then(d=>{
                        selHtml='';
                        d.data.data.map(obj=>{
                            selHtml+=`<option value="${obj.id}">${obj.name}</option>`
                        })
                        $("#state").html(selHtml)
                })
            }
            $('#state').change(function(){
                state_id = $(this).val();
                pitLib.pitFetch("POST","{{route('api.location.city.list')}}?paginate=1000",{state_id:state_id}).then(d=>{
                        selHtml='';
                        d.data.data.map(obj=>{
                            selHtml+=`<option value="${obj.id}">${obj.name}</option>`
                        })
                        $("#city").html(selHtml)
                })
            })
            {{--new Tagify($("#state")[0], {--}}
            {{--    mode : "select",--}}
            {{--    enforceWhitelist: true,--}}
            {{--    whitelist:{!! $states !!},--}}
            {{--    dropdown: {--}}
            {{--        maxItems: 20,--}}
            {{--        classname: "tags-look",--}}
            {{--        enabled: 0,--}}
            {{--        closeOnSelect: false--}}
            {{--    }--}}
            {{--})--}}
            {{--new Tagify($("#city")[0], {--}}
            {{--    mode : "select",--}}
            {{--    enforceWhitelist: true,--}}
            {{--    whitelist:{!! $cities !!},--}}
            {{--    dropdown: {--}}
            {{--        maxItems: 20,--}}
            {{--        classname: "tags-look",--}}
            {{--        enabled: 0,--}}
            {{--        closeOnSelect: false--}}
            {{--    }--}}
            {{--})--}}
        });
        ClassicEditor
            .create( document.querySelector('#description'), {
                // Editor configuration.
            } )
            .then( editor => {
                window.editor = editor;
            } )

        ClassicEditor
            .create( document.querySelector( '#houseRules' ), {
                // Editor configuration.
            } )
            .then( editor => {
                window.editor = editor;
            } )


        $("#btn_add_faq").click(function () {
            $("#tbl_faq_input").find("tbody").append(getFaqRow())
        })
        function delFaqRow(id) {
            $("#tbl_faq_input").find("#"+id).fadeOut(500,function () {
                $("#tbl_faq_input").find("#"+id).remove()
            })
        }

        $("#btn_add_specification").click(function () {
            $("#tbl_specification_input").find("tbody").append(getSpecificationRow())
        })
        function delSpecifRow(id) {
            $("#tbl_specification_input").find("#"+id).fadeOut(500,function () {
                $("#tbl_specification_input").find("#"+id).remove()
            })
        }
    </script>
@endsection
