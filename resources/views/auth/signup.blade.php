@extends('layout.auth')
@section('title','Signup')
@section('content')
    <!--begin::Authentication - Sign-up -->
    <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url(/public/panel/assets/media/illustrations/sketchy-1/14.png)">
        <!--begin::Content-->
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
            <!--begin::Logo-->
            <a href="../../demo1/dist/index.html" class="mb-12">
                <img alt="Logo" src="/public/uploads/images/logo.png" class="h-40px" />
            </a>
            @if(!request()->has('type'))
                <div class="row">
                    <center class="mb-2">
                        <h1>
                            Register As
                        </h1>
                    </center>
                    <div class="col text-center">
                        <a href="{{route('signup.view',['type'=>'agency'])}}" class="btn btn-primary btn-lg btn-block">Agency</a>
                    </div>
                    <div class="col text-center">
                        <a href="{{route('signup.view',['type'=>'agent'])}}" class="btn bg-success text-white btn-lg btn-block">Agent</a>
                    </div>
                </div>
            @else
                @if(request()->get('type')=='agency')
                    <div class="w-lg-600px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                        <!--begin::Form-->
                        <form method="post" action="{{route('signup.store')}}" class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate">
                            @csrf
                            <input type="hidden" name="type" value="{{request()->get('type')}}">
                            <!--begin::Heading-->
                            <div class="mb-10 text-center">
                                <!--begin::Title-->
                                <h1 class="text-dark mb-3">Register as <strong class="text-decoration-underline">Agency</strong></h1>
                                <!--end::Title-->
                                <!--begin::Link-->
                                <div class="text-gray-400 fw-bold fs-4">Already have an account?
                                    <a href="{{route('login.view')}}" class="link-primary fw-bolder">Sign in here</a></div>
                                <!--end::Link-->

                                <div class="row mt-2">
                                    <div class="col">
                                        @if($errors->isNotEmpty())
                                            <div class="alert alert-danger" role="alert">
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    </div>
                                </div>


                            </div>
                            <!--end::Heading-->
                            <!--begin::Input group-->
                            <div class="row fv-row mb-7 fv-plugins-icon-container">
                                <!--begin::Col-->
                                <div class="col-xl-6">
                                    <label class="form-label fw-bolder text-dark fs-6">Agency Name</label>
                                    <input value="{{old('name')}}" class="form-control form-control-lg form-control-solid @error('name') is-invalid @enderror" type="text" placeholder="" name="name" autocomplete="off">
                                    @error('name')
                                    <div class="fv-plugins-message-container invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-7 fv-plugins-icon-container">
                                <label class="form-label fw-bolder text-dark fs-6">Email</label>
                                <input value="{{old('email')}}" class="form-control form-control-lg form-control-solid @error('email') is-invalid @enderror" type="email" placeholder="" name="email" autocomplete="off">
                                @error('email')
                                <div class="fv-plugins-message-container invalid-feedback">{{ $message }}</div>
                                @enderror
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-10 fv-row fv-plugins-icon-container" >
                                    <!--begin::Wrapper-->
                                    <div class="mb-1">
                                        <!--begin::Label-->
                                        <label class="form-label fw-bolder text-dark fs-6">Password</label>
                                        <!--end::Label-->
                                        <!--begin::Input wrapper-->
                                        <div class="position-relative mb-3">
                                            <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" name="password" autocomplete="off">
                                            <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
											<i class="bi bi-eye-slash fs-2"></i>
											<i class="bi bi-eye fs-2 d-none"></i>
										</span>
                                        </div>
                                        <!--end::Input wrapper-->
                                        {{--                                    <!--begin::Meter-->--}}
                                        {{--                                    <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <!--end::Meter-->--}}
                                    </div>
                                    <!--end::Wrapper-->

                                    <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                <!--end::Input group=-->
                                <!--begin::Input group-->
                                <div class="fv-row mb-5 fv-plugins-icon-container">
                                    <label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
                                    <input class="form-control form-control-lg form-control-solid @error('password') is-invalid @enderror" type="password" placeholder="" name="password_confirmation" autocomplete="off">
                                    @error('password')
                                    <div class="fv-plugins-message-container invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <!--end::Input group-->

                                <!--begin::Actions-->
                                <div class="text-center">
                                    <button type="submit" id="kt_sign_up_submit" class="btn btn-lg btn-primary">
                                        <span class="indicator-label">Submit</span>
                                        <span class="indicator-progress">Please wait...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                                <!--end::Actions-->
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                @endif

                @if(request()->get('type')=='agent')
                    <div class="w-lg-600px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                        <!--begin::Form-->
                        <form method="post" action="{{route('signup.store')}}" class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate">
                            @csrf
                            <input type="hidden" name="type" value="{{request()->get('type')}}">
                            <!--begin::Heading-->
                            <div class="mb-10 text-center">
                                <!--begin::Title-->
                                <h1 class="text-dark mb-3">Register as <strong class="text-decoration-underline">Agent</strong></h1>
                                <!--end::Title-->
                                <!--begin::Link-->
                                <div class="text-gray-400 fw-bold fs-4">Already have an account?
                                    <a href="{{route('login.view')}}" class="link-primary fw-bolder">Sign in here</a></div>
                                <!--end::Link-->

                                <div class="row mt-2">
                                    <div class="col">
                                        @if($errors->isNotEmpty())
                                            <div class="alert alert-danger" role="alert">
                                                {{$errors->first()}}
                                            </div>
                                        @endif
                                    </div>
                                </div>


                            </div>
                            <!--end::Heading-->
                            <!--begin::Input group-->
                            <div class="row fv-row mb-7 fv-plugins-icon-container">
                                <!--begin::Col-->
                                <div class="col-xl-6">
                                    <label class="form-label fw-bolder text-dark fs-6">Agent Name</label>
                                    <input value="{{old('name')}}" class="form-control form-control-lg form-control-solid @error('name') is-invalid @enderror" type="text" placeholder="" name="name" autocomplete="off">
                                    @error('name')
                                    <div class="fv-plugins-message-container invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-7 fv-plugins-icon-container">
                                <label class="form-label fw-bolder text-dark fs-6">Email</label>
                                <input value="{{old('email')}}" class="form-control form-control-lg form-control-solid @error('email') is-invalid @enderror" type="email" placeholder="" name="email" autocomplete="off">
                                @error('email')
                                <div class="fv-plugins-message-container invalid-feedback">{{ $message }}</div>
                                @enderror
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="mb-10 fv-row fv-plugins-icon-container" >
                                    <!--begin::Wrapper-->
                                    <div class="mb-1">
                                        <!--begin::Label-->
                                        <label class="form-label fw-bolder text-dark fs-6">Password</label>
                                        <!--end::Label-->
                                        <!--begin::Input wrapper-->
                                        <div class="position-relative mb-3">
                                            <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" name="password" autocomplete="off">
                                            <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
											<i class="bi bi-eye-slash fs-2"></i>
											<i class="bi bi-eye fs-2 d-none"></i>
										</span>
                                        </div>
                                        <!--end::Input wrapper-->
                                        {{--                                    <!--begin::Meter-->--}}
                                        {{--                                    <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>--}}
                                        {{--                                        <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>--}}
                                        {{--                                    </div>--}}
                                        {{--                                    <!--end::Meter-->--}}
                                    </div>
                                    <!--end::Wrapper-->

                                    <div class="fv-plugins-message-container invalid-feedback"></div></div>
                                <!--end::Input group=-->
                                <!--begin::Input group-->
                                <div class="fv-row mb-5 fv-plugins-icon-container">
                                    <label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
                                    <input class="form-control form-control-lg form-control-solid @error('password') is-invalid @enderror" type="password" placeholder="" name="password_confirmation" autocomplete="off">
                                    @error('password')
                                    <div class="fv-plugins-message-container invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <!--end::Input group-->

                                <!--begin::Actions-->
                                <div class="text-center">
                                    <button type="submit" id="kt_sign_up_submit" class="btn btn-lg btn-primary">
                                        <span class="indicator-label">Submit</span>
                                        <span class="indicator-progress">Please wait...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                                    </button>
                                </div>
                                <!--end::Actions-->
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                @endif
            @endif

        </div>
        <!--end::Content-->
        <!--begin::Footer-->
        <div class="d-flex flex-center flex-column-auto p-10">
            <!--begin::Links-->
            <div class="d-flex align-items-center fw-bold fs-6">
                <a href="https://keenthemes.com" class="text-muted text-hover-primary px-2">About</a>
                <a href="mailto:support@keenthemes.com" class="text-muted text-hover-primary px-2">Contact</a>
                <a href="https://1.envato.market/EA4JP" class="text-muted text-hover-primary px-2">Contact Us</a>
            </div>
            <!--end::Links-->
        </div>
        <!--end::Footer-->
    </div>
    <!--end::Authentication - Sign-up-->
@endsection
