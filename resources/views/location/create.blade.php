@extends('layout.panel')
@section('breadcrumb')
    <li class="breadcrumb-item pe-3"><a href="{{route('location.list.view')}}" class="pe-3">Location</a></li>
    <li class="breadcrumb-item pe-3">Create</li>
@endsection
@section('content')
    <!--begin::Post-->
    <div class="row">
        <div class="col">
            <div class="card card-custom">
                <form action="{{route('location.create')}}"  method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <div class="card-title">
                        <span class="card-icon">
                            <i class="flaticon2-chat-1 text-primary"></i>
                        </span>
                            <h3 class="card-label">
                                Create New Location
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{route('location.list.view')}}" class="btn btn-sm btn-light-primary font-weight-bold">
                                <i class="flaticon2-cube"></i> List
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if($errors->isNotEmpty()||session('error'))
                            <div class="row mt-4">
                                <div class="col">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <ul>
                                            @if($errors->isNotEmpty())
                                                <li>{{$errors->first()}}</li>
                                            @endif
                                            @if(session('error'))
                                                    <li>{{session('error')}}</li>
                                            @endif
                                        </ul>
                                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input value="{{old('title')}}" id="title" name="title" class="form-control" type="text" placeholder="Insert Loaction"/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <span></span>
                        <button class="btn btn-md btn-primary" type="submit">
                            Save
                            <i class="fa fa-solid fa-check ms-2"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end::Post-->
@endsection
@section('page_script')
    <script>
        function getFaqRow() {
            curId=pitLib.util.uniquid()
            return `
        <tr id='${curId}'>
            <td class="col-4">
                <input class='form-control' type="text" name="faq_q[]" placeholder="Ex. Can I bring my pet?">
            </td>
            <td class="col">
                <input class='form-control' type="text" name="faq_a[]" placeholder="Yes Pets are allowed">
            </td>
            <td>
                <button onclick="delFaqRow('${curId}')" type="button" class="btn btn-danger btn-sm rounded btn_del_faq_row">Delete</button>
            </td>
         </tr>
        `
        }
        $( document ).ready(function() {
        });

    </script>
@endsection
