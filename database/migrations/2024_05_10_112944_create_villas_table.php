<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('villas', function (Blueprint $table) {
            $table->id();
            $table->string('name',512);
            $table->text('description')->nullable();
            $table->text('houseRules')->nullable();
            $table->string('country',128)->nullable();
            $table->string('state',128)->nullable();
            $table->string('city',128)->nullable();
            $table->string('countryCode',8)->nullable();
            $table->string('zipcode',8)->nullable();
            $table->string('price',32)->nullable();
            $table->decimal('starRating',8,2)->nullable();
            $table->string('weeklyDiscount',64)->nullable();
            $table->string('monthlyDiscount',64)->nullable();
            $table->string('address',512)->nullable();
            $table->string('lat',128)->nullable();
            $table->string('lng',128)->nullable();
            $table->string('roomType',128)->nullable();
            $table->string('language',8)->nullable();
            $table->string('currencyCode',8)->nullable();
            $table->json('listingAmenities')->nullable();
            $table->enum('status',['pending','published'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('villas');
    }
};
