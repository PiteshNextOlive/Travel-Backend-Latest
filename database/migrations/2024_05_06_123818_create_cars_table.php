<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('title',256);
            $table->string('slug',256)->unique();
            $table->text('content')->nullable();
            $table->enum('status',['pending','published'])->default('pending');
            $table->bigInteger('type')->nullable()->comment('car types will be fetched from EXTRAS');
            $table->json('features')->nullable();
            $table->bigInteger('location')->nullable();
            $table->string('address',512)->nullable();
            $table->string('youtube_link',512)->nullable();
            $table->string('vin',512)->nullable()->comment('Car Number (may be license etc)');
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();

            $table->decimal('base_price_daily',8,2)->nullable();
            $table->enum('price_mod_sign_daily',['+','-'])->nullable();
            $table->enum('price_mod_type_daily',['f','p'])->nullable()->comment('Flat and Percent');
            $table->decimal('price_mod_amount_daily',8,2)->nullable();

            $table->decimal('base_price_weekly',8,2)->nullable();
            $table->enum('price_mod_sign_weekly',['+','-'])->nullable();
            $table->enum('price_mod_type_weekly',['f','p'])->nullable()->comment('Flat and Percent');
            $table->decimal('price_mod_amount_weekly',8,2)->nullable();


            $table->decimal('base_price_monthly',8,2)->nullable();
            $table->enum('price_mod_sign_monthly',['+','-'])->nullable();
            $table->enum('price_mod_type_monthly',['f','p'])->nullable()->comment('Flat and Percent');
            $table->decimal('price_mod_amount_monthly',8,2)->nullable();


            $table->enum('gear_type',['automatic','manual'])->nullable();
            $table->integer('door')->nullable();

            $table->json('specification')->nullable();
            $table->json('faq')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
