<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->id();
            $table->string('iata',4);
            $table->string('lat',16)->nullable();
            $table->string('lon',16)->nullable();
            $table->string('iso',2);
            $table->tinyInteger('status');
            $table->string('name',128)->nullable();
            $table->string('continent',2);
            $table->string('type',16);
            $table->string('size',8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('airports');
    }
};
