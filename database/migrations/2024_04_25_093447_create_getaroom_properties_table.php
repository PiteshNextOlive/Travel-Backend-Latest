<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('getaroom_properties', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->string('permalink',128);
            $table->string('title',256);
            $table->decimal('lat',11,8)->nullable();
            $table->decimal('lng',11,8)->nullable();
            $table->string('short-description',512)->nullable();
            $table->text('sanitized-description')->nullable();
            $table->decimal('rating',2,1)->nullable();
            $table->string('location-phone',128)->nullable();
            $table->string('location-street',256)->nullable();
            $table->string('location-city',128)->nullable();
            $table->string('location-state',128)->nullable();
            $table->string('location-zip',128)->nullable();
            $table->string('location-country',8)->nullable();
            $table->string('thumbnail-filename',512)->nullable();
            $table->text('amenities')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('getaroom_properties');
    }
};
