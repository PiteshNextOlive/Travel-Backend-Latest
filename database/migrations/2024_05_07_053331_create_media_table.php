<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('path',2048)->comment('Path of file');
            $table->boolean('is_local')->default(true)->comment('File is stored in local or its a http link');
            $table->string('reference_code',256)->nullable()->comment('Reference code example: USER_PROFILE_PIC');
            $table->string('refrence_id',10)->nullable()->comment('Target id example 7');
            $table->string('additional',1024)->nullable()->comment('Any additional information');
            $table->string('created_by',10)->nullable()->comment('Created by user id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media');
    }
};
