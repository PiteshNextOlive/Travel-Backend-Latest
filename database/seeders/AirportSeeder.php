<?php

namespace Database\Seeders;

use App\Models\Airport;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (\File::exists(public_path('uploads/json/airports.json'))){
            $data=json_decode(\File::get(public_path('uploads/json/airports.json')));
            foreach ($data as $d){
                Airport::insert((array)$d);
            }
        }
    }
}
