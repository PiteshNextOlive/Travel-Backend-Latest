<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::insert([
            ['name'=>'Super Admin','phone'=>'1234567890','email'=>'admin@mail.com','password'=>\Hash::make('1234')]
        ]);
        $u=User::first();
        $r=Role::first();
        $u->assignRole('super_admin');
    }
}
