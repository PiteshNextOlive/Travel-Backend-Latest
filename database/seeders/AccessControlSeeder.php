<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AccessControlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::insert([
            ['name' => 'super_admin','guard_name'=>'web'],
            ['name' => 'agency','guard_name'=>'web'],
            ['name' => 'agent','guard_name'=>'web'],
        ]);
        Permission::insert([

        ]);
    }
}
